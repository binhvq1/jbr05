package com.devcamp.countryregion.controller;

import java.util.ArrayList;

import com.devcamp.countryregion.model.Region;
import com.devcamp.countryregion.service.RegionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class RegionController {
    @Autowired
    private RegionService regionService;

    @GetMapping("/region-info")
    public Region getRegionInfo(@RequestParam(required = true, name="regionCode") String regionCode) {
        ArrayList<Region> regions = regionService.getAllRegion();

        Region findRegion = new Region();
        for (Region region : regions) {
            if(region.getRegionCode().equals(regionCode)) {
                findRegion = region;
            }
        }

        return findRegion;
    }
}
