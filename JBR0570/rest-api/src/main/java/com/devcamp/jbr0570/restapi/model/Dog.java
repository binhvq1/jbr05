package com.devcamp.jbr0570.restapi.model;

public class Dog extends Mammal {

    public Dog() {
        super();
    }

    public Dog(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Dog []";
    }

    
}
