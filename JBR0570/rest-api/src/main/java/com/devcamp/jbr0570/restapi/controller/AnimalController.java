package com.devcamp.jbr0570.restapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import com.devcamp.jbr0570.restapi.model.Animal;
import com.devcamp.jbr0570.restapi.model.Cat;
import com.devcamp.jbr0570.restapi.service.AnimalService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AnimalController {
    @Autowired
    private AnimalService animalService;
    @GetMapping("/cats")
    public ArrayList<Animal> getDanhSAchMeoAPi(){
        ArrayList<Animal> DanhSach = animalService.getAnimalAll();
        ArrayList<Animal> DanhSachMeo = new ArrayList<>();
        for (Animal animal : DanhSach) {
            if(animal instanceof Cat){
                DanhSachMeo.add(animal);
            }
        }
        return DanhSachMeo ;
    }
}
