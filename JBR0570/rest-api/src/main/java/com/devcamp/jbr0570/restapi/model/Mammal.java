package com.devcamp.jbr0570.restapi.model;

public class Mammal extends Animal {

    public Mammal() {
        super();
    }

    public Mammal(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Mammal []";
    }
    
    
}
