package com.devcamp.jbr0570.restapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.jbr0570.restapi.model.Animal;
import com.devcamp.jbr0570.restapi.model.Cat;
import com.devcamp.jbr0570.restapi.model.Dog;

@Service
public class AnimalService {
    Cat cat1 = new Cat("Mèo Vàng");
    Cat cat2 = new Cat("Mèo Đen");
    Cat cat3 = new Cat("Mèo Xám");

    Dog dog1 = new Dog("Chó Vàng");
    Dog dog2 = new Dog("Chó Đen");
    Dog dog3 = new Dog("Chó Xám");


    public ArrayList<Animal> getAnimalAll(){
        ArrayList<Animal> AllAnimal = new ArrayList<>();

        AllAnimal.add(cat1);
        AllAnimal.add(cat2);
        AllAnimal.add(cat3);
        AllAnimal.add(dog1);
        AllAnimal.add(dog2);
        AllAnimal.add(dog3);

        return AllAnimal ;
    }


}
