package com.devcamp.jbr0570.restapi.model;

public class Cat extends Mammal {
    
    public Cat() {
        super();
    }

    public Cat(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Cat []";
    }

    
    
}
