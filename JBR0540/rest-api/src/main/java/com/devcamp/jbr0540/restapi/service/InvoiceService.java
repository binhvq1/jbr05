package com.devcamp.jbr0540.restapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.jbr0540.restapi.model.Invoice;

@Service
public class InvoiceService {
    CustomerService customerService = new CustomerService();
    Invoice invoice1 = new Invoice(1, customerService.customer1, 1.0);

    Invoice invoice2 = new Invoice(2, customerService.customer2, 2.0);
    
    Invoice invoice3 = new Invoice(3, customerService.customer3, 3.0);

    public ArrayList<Invoice> getAllInvoice() {
        ArrayList<Invoice> allInvoice = new ArrayList<Invoice>();
        allInvoice.add(invoice1);
        allInvoice.add(invoice2);
        allInvoice.add(invoice3);
        return allInvoice;
        
    }
}
