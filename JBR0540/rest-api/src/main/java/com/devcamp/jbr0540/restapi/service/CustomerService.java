package com.devcamp.jbr0540.restapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.jbr0540.restapi.model.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer(1, "Hung", 10);
    Customer customer2 = new Customer(2, "Nam", 20);
    Customer customer3 = new Customer(3, "Phuong", 10);
    Customer customer4 = new Customer(4, "Binh", 15);
    Customer customer5 = new Customer(5, "Toan", 10);
    Customer customer6 = new Customer(6, "Thang", 10);

    public ArrayList<Customer> getAllCustomer() {
        ArrayList<Customer> allCustomer = new ArrayList<Customer>();
        allCustomer.add(customer1);
        allCustomer.add(customer2);
        allCustomer.add(customer3);
        allCustomer.add(customer4);
        allCustomer.add(customer5);
        allCustomer.add(customer6);
        return allCustomer;
    }
}
