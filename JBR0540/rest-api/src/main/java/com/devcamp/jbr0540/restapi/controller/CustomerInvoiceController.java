package com.devcamp.jbr0540.restapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr0540.restapi.service.CustomerService;
import com.devcamp.jbr0540.restapi.service.InvoiceService;
import com.devcamp.jbr0540.restapi.model.Customer;
import com.devcamp.jbr0540.restapi.model.Invoice;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CustomerInvoiceController {
    @Autowired
    private InvoiceService invoiceService;
    @GetMapping("/invoices")
    public ArrayList<Invoice> getAllInvoice() {
        ArrayList<Invoice> allInvoice = invoiceService.getAllInvoice();
        return allInvoice;
    }

    @Autowired
    private CustomerService customerService;
    @GetMapping("/customers")
    public ArrayList<Customer> getAllCustomer() {
        ArrayList<Customer> allCustomer = customerService.getAllCustomer();
        return allCustomer;
    }
}
