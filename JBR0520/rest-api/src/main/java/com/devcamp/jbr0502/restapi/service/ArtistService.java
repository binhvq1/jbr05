package com.devcamp.jbr0502.restapi.service;

import java.util.ArrayList;
import com.devcamp.jbr0502.restapi.model.Album;
import com.devcamp.jbr0502.restapi.model.Artist;

import org.springframework.stereotype.Service;

@Service
public class ArtistService {
    AlbumService albumService = new AlbumService();
    ArrayList<Album> album01 = albumService.getListAlbum01();
    ArrayList<Album> album02 = albumService.getListAlbum02();
    ArrayList<Album> album03 = albumService.getListAlbum03();

    Artist artist01 = new Artist(01, "Hoang", album01);
    Artist artist02 = new Artist(02, "Nam", album02);
    Artist artist03 = new Artist(03, "Tien", album03);

    public ArrayList<Artist> getAllArtist() {
        ArrayList<Artist> listArtist = new ArrayList<Artist>();
        listArtist.add(artist01);
        listArtist.add(artist02);
        listArtist.add(artist03);
        return listArtist;
    }
    
}
