package com.devcamp.jbr0502.restapi.service;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.stereotype.Service;

import com.devcamp.jbr0502.restapi.model.Album;

@Service
public class AlbumService {
    Album album1 = new Album(01, "Hung", new ArrayList<>(Arrays.asList("Bai ca01","Bai ca 02","Bai ca 03")));
    Album album2 = new Album(02, "Hung", new ArrayList<>(Arrays.asList("Bai ca01","Bai ca 02","Bai ca 03")));
    Album album3 = new Album(03, "Hung", new ArrayList<>(Arrays.asList("Bai ca01","Bai ca 02","Bai ca 03")));
    
    public ArrayList<Album> getListAlbum01() {
        ArrayList<Album> listAlbum = new ArrayList<Album>();
        listAlbum.add(album1);
        listAlbum.add(album2);
        listAlbum.add(album3);
        return listAlbum;
    }

    Album album4 = new Album(04, "Hung", new ArrayList<>(Arrays.asList("Bai ca04","Bai ca 05","Bai ca 06")));
    Album album5 = new Album(05, "Hung", new ArrayList<>(Arrays.asList("Bai ca04","Bai ca 05","Bai ca 06")));
    Album album6 = new Album(06, "Hung", new ArrayList<>(Arrays.asList("Bai ca04","Bai ca 05","Bai ca 06")));
    
    public ArrayList<Album> getListAlbum02() {
        ArrayList<Album> listAlbum02 = new ArrayList<Album>();
        listAlbum02.add(album4);
        listAlbum02.add(album5);
        listAlbum02.add(album6);
        return listAlbum02;
    }

    Album album7 = new Album(07, "Hung", new ArrayList<>(Arrays.asList("Bai ca04","Bai ca 05","Bai ca 06")));
    Album album8 = new Album(10, "Hung", new ArrayList<>(Arrays.asList("Bai ca04","Bai ca 05","Bai ca 06")));
    Album album9 = new Album(11, "Hung", new ArrayList<>(Arrays.asList("Bai ca04","Bai ca 05","Bai ca 06")));
    
    public ArrayList<Album> getListAlbum03() {
        ArrayList<Album> listAlbum03 = new ArrayList<Album>();
        listAlbum03.add(album7);
        listAlbum03.add(album8);
        listAlbum03.add(album9);
        return listAlbum03;
    }

    public ArrayList<Album> getAllAlbum() {
        ArrayList<Album> albums = new ArrayList<>();
        
        albums.add(album1);
        albums.add(album2);
        albums.add(album3);
        albums.add(album4);
        albums.add(album5);
        albums.add(album6);
        albums.add(album7);
        albums.add(album8);
        albums.add(album9);

        return albums;
    }
    
}
