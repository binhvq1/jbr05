package com.devcamp.jbr0502.restapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr0502.restapi.model.Album;
import com.devcamp.jbr0502.restapi.model.Artist;
import com.devcamp.jbr0502.restapi.service.AlbumService;
import com.devcamp.jbr0502.restapi.service.ArtistService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class ArtistAlbumController {
    @Autowired
    private ArtistService artistService;
    @GetMapping("/artists")
    public ArrayList<Artist> getAllCountries() {
        ArrayList<Artist> allArtist = artistService.getAllArtist();
        return allArtist;
    }

    @GetMapping("/artist-info")
    public Artist getArtistInfo(@RequestParam(name="artistid") int id) {
        ArrayList<Artist> allArtist = artistService.getAllArtist();

        Artist findArtist = new Artist();

        for (Artist artist : allArtist) {
            if(artist.getId() == id) {
                findArtist = artist;
            }
        }

        return findArtist;
    }

    @Autowired
    private AlbumService albumService;
    @GetMapping("/album-info")
    public Album getAlbumInfo(@RequestParam(name="albumid") int id) {
        ArrayList<Album> allAlbum = albumService.getAllAlbum();

        Album findAlbum = new Album();

        for (Album album : allAlbum) {
            if(album.getId() == id) {
                findAlbum = album;
            }
        }
        return findAlbum;
    }
    
}
