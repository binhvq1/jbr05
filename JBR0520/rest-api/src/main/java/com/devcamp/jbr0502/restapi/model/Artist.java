package com.devcamp.jbr0502.restapi.model;

import java.util.ArrayList;

public class Artist {
    int id;
    String name;
    ArrayList<Album> albums;
    
    public Artist() {
    }

    public Artist(int id, String name, ArrayList<Album> albums) {
        this.id = id;
        this.name = name;
        this.albums = albums;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(ArrayList<Album> albums) {
        this.albums = albums;
    }
}
