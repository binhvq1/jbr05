package com.devcamp.jbr580.models;

import java.util.ArrayList;

public class Album {
    private String name;
    private ArrayList<String> songs;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public ArrayList<String> getSongs() {
        return songs;
    }
    public void setSongs(ArrayList<String> songs) {
        this.songs = songs;
    }
    public Album(String name, ArrayList<String> songs) {
        this.name = name;
        this.songs = songs;
    }
    @Override
    public String toString() {
        return "Album [name=" + name + ", songs=" + songs + "]";
    }
    
}
