package com.devcamp.jbr580;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr580Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr580Application.class, args);
	}

}
