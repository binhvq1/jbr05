package com.devcamp.jbr580.services;
import java.util.ArrayList;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.jbr580.models.Artist;
import com.devcamp.jbr580.models.Band;
import com.devcamp.jbr580.models.BandMember;
import com.devcamp.jbr580.models.Composer;


@Service
public class ComposerService {
    @Autowired
    private AlbumServices albumServices;
    public ArrayList<Composer> getComposer() {
        ////////////////////////////////Artists////////////////////////////////
        Artist artist1 = new Artist("Nguyen Van", "A", "An void", albumServices.getAlbums());
        Artist artist2 = new Artist("Bui Van ", "B", albumServices.getAlbums());
        Artist artist3 = new Artist("Trương Tam", "Phong", albumServices.getAlbums());
        
        ////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////BandMembers/////////////////////////////////////////
       BandMember bandMember1 = new BandMember("Pham Trong ", "Văn", "Văn Vui Vẻ", "Drum");
       BandMember bandMember2 = new BandMember("P ", "Văn", "Văn Vui Vẻ", "Drum");
       BandMember bandMember3 = new BandMember("Pham Trong ", "Văn", "Văn Vui Vẻ", "Drum");
       BandMember bandMember4 = new BandMember("Pham Trong ", "Văn", "Văn Vui Vẻ", "Drum");
       BandMember bandMember5 = new BandMember("Pham Trong ", "Văn", "Văn Vui Vẻ", "Drum");
       BandMember bandMember6 = new BandMember("Pham Trong ", "Văn", "Văn Vui Vẻ", "Drum");
      
        //////////////////////  //////////////////////////////////////////////////////////////////////////////////
        ArrayList<Composer> listComposer = new ArrayList<Composer>();
        listComposer.add(artist1);
        listComposer.add(artist3);
        listComposer.add(artist2);
        
        listComposer.add(bandMember1);
        listComposer.add(bandMember2);
        listComposer.add(bandMember3);
        listComposer.add(bandMember4);
        listComposer.add(bandMember5);
        listComposer.add(bandMember6);
        return listComposer;
    }
    public ArrayList<Artist> getArtistList() {
        ////////////////////////////////Artists////////////////////////////////
        Artist artist1 = new Artist("Bui Van", "An", "An void", albumServices.getAlbums());
        Artist artist2 = new Artist("Bui Van ", "Toàn", albumServices.getAlbums());
        Artist artist3 = new Artist("Trương Thị Kim", "Hương", albumServices.getAlbums());
        
        ////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////BandMembers/////////////////////////////////////////
      
        ArrayList<Artist> listComposer = new ArrayList<Artist>();
        listComposer.add(artist1);
        listComposer.add(artist3);
        listComposer.add(artist2);
        return listComposer;
    }
    public ArrayList<Band> getBand() {
        ////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////BandMembers/////////////////////////////////////////
       BandMember bandMember1 = new BandMember("Pham Trong ", "Văn", "Văn Vui Vẻ", "Drum");
       BandMember bandMember2 = new BandMember("P ", "Văn", "Văn Vui Vẻ", "Drum");
       BandMember bandMember3 = new BandMember("Pham Trong ", "Văn", "Văn Vui Vẻ", "Drum");
       BandMember bandMember4 = new BandMember("Pham Trong ", "Văn", "Văn Vui Vẻ", "Drum");
       BandMember bandMember5 = new BandMember("Pham Trong ", "Văn", "Văn Vui Vẻ", "Drum");
       
      
        //////////////////////  //////////////////////////////////////////////////////////////////////////////////
        ArrayList<BandMember> listBandMembers = new ArrayList<BandMember>();
        listBandMembers.add(bandMember1);
        listBandMembers.add(bandMember2);
        listBandMembers.add(bandMember3);
        listBandMembers.add(bandMember4);
        listBandMembers.add(bandMember5);
        ArrayList<BandMember> listBandMembers1 = new ArrayList<BandMember>();
        listBandMembers1.add(bandMember1);
        listBandMembers1.add(bandMember3);
        
        ArrayList<Band> listBand = new ArrayList<Band>();
        Band nhom3conmeo = new Band("3conmeo", listBandMembers, albumServices.getAlbums());
        Band nhom3concho = new Band("3concho", listBandMembers1, albumServices.getAlbums());
        listBand.add(nhom3conmeo);
        listBand.add(nhom3concho);
        
        
        
        return listBand;
    }
    
}