package com.devcamp.jbr580.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr580.models.Album;
import com.devcamp.jbr580.services.AlbumServices;

@RestController
@CrossOrigin
public class AlbumController {
  
    @Autowired
    private AlbumServices albumServices;
    @GetMapping(value="/album")
    public ArrayList<Album> getAlbums() {

        ArrayList<Album> albums = albumServices.getAlbums();
        return albums;
    }
    
    
}
