package com.devcamp.jbr580.models;

public class BandMember extends Composer{

    public BandMember(String firstname, String lastname, String stageName) {
        super(firstname, lastname, stageName);
        //TODO Auto-generated constructor stub
    }
    private String instrument;
    public String getInstrument() {
        return instrument;
    }
    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }
  
    public BandMember(String firstname, String lastname, String stageName, String instrument) {
        super(firstname, lastname, stageName);
        this.instrument = instrument;
    }
    @Override
    public String toString() {
        return "BandMember [instrument=" + instrument + "]";
    }
    
}
