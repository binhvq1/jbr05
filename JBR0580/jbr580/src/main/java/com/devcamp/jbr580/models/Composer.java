package com.devcamp.jbr580.models;

public class Composer extends Person{

    public Composer(String firstname, String lastname) {
        super(firstname, lastname);
        //TODO Auto-generated constructor stub
    }
    private String stageName;
    public String getStageName() {
        return stageName;
    }
    public void setStageName(String stageName) {
        this.stageName = stageName;
    }
    public Composer(String firstname, String lastname, String stageName) {
        super(firstname, lastname);
        this.stageName = stageName;
    }
    
}
