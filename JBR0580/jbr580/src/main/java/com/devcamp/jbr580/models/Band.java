package com.devcamp.jbr580.models;

import java.util.ArrayList;

public class Band {
    private String bandName;
    private ArrayList<BandMember> members ;
    private ArrayList<Album> albums ;
    public String getBandName() {
        return bandName;
    }
    public void setBandName(String bandName) {
        this.bandName = bandName;
    }
    public ArrayList<BandMember> getMembers() {
        return members;
    }
    public void setMembers(ArrayList<BandMember> members) {
        this.members = members;
    }
    public ArrayList<Album> getAlbums() {
        return albums;
    }
    public void setAlbums(ArrayList<Album> albums) {
        this.albums = albums;
    }
    public Band(String bandName, ArrayList<BandMember> members, ArrayList<Album> albums) {
        this.bandName = bandName;
        this.members = members;
        this.albums = albums;
    }
    ////////////////////////////
    public void AddAlbum(Album album) {
        this.albums.add(album);
    }
    /////////////////////////////

    @Override
    public String toString() {
        return "Band [albums=" + albums + ", bandName=" + bandName + ", members=" + members + "]";
    }
}
