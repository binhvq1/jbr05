package com.devcamp.jbr580.models;

import java.util.ArrayList;

public class Artist extends Composer{

    public Artist(String firstname, String lastname) {
        super(firstname, lastname);
        //TODO Auto-generated constructor stub
    }
    
    private ArrayList<Album> albums ;
    public ArrayList<Album> getAlbums() {
        return albums;
    }
    public void setAlbums(ArrayList<Album> albums) {
        this.albums = albums;
    }
    public Artist(String firstname, String lastname, ArrayList<Album> albums) {
        super(firstname, lastname);
        this.albums = albums;
    }
    public Artist(String firstname, String lastname, String stageName, ArrayList<Album> albums) {
        super(firstname, lastname, stageName);
        this.albums = albums;
    }
    
}
