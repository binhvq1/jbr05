package com.devcamp.jbr580.services;

import java.util.ArrayList;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.jbr580.models.Album;

@Service
public class AlbumServices {
    @Autowired
    public ArrayList<Album> getAlbums() {
        //////////////////////// album1//////////////////
        String baihat1 ="Bài hát 1";
        String baihat2 ="Bài hát 2";
        String baihat3 ="Bài hát 3";
        String baihat4 ="Bài hát 4";
        String baihat5 ="Bài hát 5";
        String baihat6 ="Bài hát 6";
        String baihat7 ="Bài hát 7";
        String baihat8 ="Bài hát 8";
        String baihat9 ="Bài hát 9";
        String baihat10 ="Bài hát 10";
        String baihat11 ="Bài hát 11";
        String baihat12="Bài hát 12";
        String baihat13 ="Bài hát 13";
        String baihat14 ="Bài hát 14";
        ArrayList<String> danhsachbaihat1 = new ArrayList<String>();
        danhsachbaihat1.add(baihat1);
        danhsachbaihat1.add(baihat2);
        danhsachbaihat1.add(baihat3);
        danhsachbaihat1.add(baihat6);
        danhsachbaihat1.add(baihat10);
        Album album1 = new Album("Tuyển tập album hay nhất năm 2001", danhsachbaihat1);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
         //////////////////////// album2//////////////////
     
         ArrayList<String> danhsachbaihat2 = new ArrayList<String>();
         danhsachbaihat2.add(baihat11);
         danhsachbaihat2.add(baihat3);
         danhsachbaihat2.add(baihat4);
         danhsachbaihat2.add(baihat5);
         danhsachbaihat2.add(baihat9);
         Album album2 = new Album("Tuyển tập album hay nhất năm 2002", danhsachbaihat2);
         ////////////////////////////////////////////////////////////////////////////////////////////////////////
         //////////////////////// album3//////////////////
     
         ArrayList<String> danhsachbaihat3 = new ArrayList<String>();
         danhsachbaihat3.add(baihat11);
         danhsachbaihat3.add(baihat3);
         danhsachbaihat3.add(baihat4);
         danhsachbaihat3.add(baihat5);
         danhsachbaihat3.add(baihat9);
         Album album3 = new Album("Tuyển tập album hay nhất năm 2003", danhsachbaihat3);
         ////////////////////////////////////////////////////////////////////////////////////////////////////////
           //////////////////////// album4//////////////////
     
           ArrayList<String> danhsachbaihat4 = new ArrayList<String>();
           danhsachbaihat4.add(baihat8);
           danhsachbaihat4.add(baihat2);
           danhsachbaihat4.add(baihat1);
           danhsachbaihat4.add(baihat12);
           danhsachbaihat4.add(baihat13);
           Album album4 = new Album("Tuyển tập album hay nhất năm 2004", danhsachbaihat4);
           ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////// album5//////////////////
     
            ArrayList<String> danhsachbaihat5 = new ArrayList<String>();
            danhsachbaihat5.add(baihat9);
            danhsachbaihat5.add(baihat10);
            danhsachbaihat5.add(baihat3);
            danhsachbaihat5.add(baihat14);
            danhsachbaihat5.add(baihat8);
            Album album5 = new Album("Tuyển tập album hay nhất năm 2005", danhsachbaihat5);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////// album6//////////////////
     
            ArrayList<String> danhsachbaihat6 = new ArrayList<String>();
            danhsachbaihat6.add(baihat10);
            danhsachbaihat6.add(baihat12);
            danhsachbaihat6.add(baihat3);
            danhsachbaihat6.add(baihat14);
            danhsachbaihat6.add(baihat8);
            danhsachbaihat6.add(baihat3);
            danhsachbaihat6.add(baihat5);
            danhsachbaihat6.add(baihat1);
            Album album6 = new Album("Tuyển tập album hay nhất năm 2006", danhsachbaihat6);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////// album7//////////////////
     
            ArrayList<String> danhsachbaihat7 = new ArrayList<String>();
            danhsachbaihat7.add(baihat11);
            danhsachbaihat7.add(baihat2);
            danhsachbaihat7.add(baihat3);
            danhsachbaihat7.add(baihat5);
            danhsachbaihat7.add(baihat7);
            danhsachbaihat7.add(baihat13);
            danhsachbaihat7.add(baihat8);
            danhsachbaihat7.add(baihat9);
            Album album7 = new Album("Tuyển tập album hay nhất năm 2007", danhsachbaihat7);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////// album8//////////////////
     
            ArrayList<String> danhsachbaihat8 = new ArrayList<String>();
            danhsachbaihat8.add(baihat3);
            danhsachbaihat8.add(baihat1);
            danhsachbaihat8.add(baihat9);
            danhsachbaihat8.add(baihat11);
            danhsachbaihat8.add(baihat10);
            danhsachbaihat8.add(baihat14);
            danhsachbaihat8.add(baihat2);
            danhsachbaihat8.add(baihat4);
            Album album8 = new Album("Tuyển tập album hay nhất năm 2008", danhsachbaihat8);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////// album8//////////////////
     
            ArrayList<String> danhsachbaihat9 = new ArrayList<String>();
            danhsachbaihat9.add(baihat2);
            danhsachbaihat9.add(baihat1);
            danhsachbaihat9.add(baihat9);
            danhsachbaihat9.add(baihat11);
            danhsachbaihat9.add(baihat10);
            danhsachbaihat9.add(baihat14);
            danhsachbaihat9.add(baihat2);
            danhsachbaihat9.add(baihat4);
            Album album9 = new Album("Tuyển tập album hay nhất năm 2009", danhsachbaihat9);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////// album8//////////////////
     
            ArrayList<String> danhsachbaihat10 = new ArrayList<String>();
            danhsachbaihat10.add(baihat2);
            danhsachbaihat10.add(baihat1);
            danhsachbaihat10.add(baihat9);
            danhsachbaihat10.add(baihat11);
            danhsachbaihat10.add(baihat10);
            danhsachbaihat10.add(baihat14);
            danhsachbaihat10.add(baihat2);
            danhsachbaihat10.add(baihat4);
            Album album10 = new Album("Tuyển tập album hay nhất năm 2022", danhsachbaihat10);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////


        ArrayList<Album> lstAlbums = new ArrayList<>();
        lstAlbums.add(album1);
        lstAlbums.add(album2);
        lstAlbums.add(album3);
        lstAlbums.add(album4);
        lstAlbums.add(album5);
        lstAlbums.add(album6);
        lstAlbums.add(album7);
        lstAlbums.add(album8);
        lstAlbums.add(album9);
        lstAlbums.add(album10);
        return lstAlbums;
    }


    public ArrayList<Album> getAlbum() {
        //////////////////////// album1//////////////////
        String baihat1 ="Bài hát 1";
        String baihat2 ="Bài hát 2";
        String baihat3 ="Bài hát 3";
        String baihat4 ="Bài hát 4";
        String baihat5 ="Bài hát 5";
        String baihat6 ="Bài hát 6";
        String baihat7 ="Bài hát 7";
        String baihat8 ="Bài hát 8";
        String baihat9 ="Bài hát 9";
        String baihat10 ="Bài hát 10";
        String baihat11 ="Bài hát 11";
        String baihat12="Bài hát 12";
        String baihat13 ="Bài hát 13";
        String baihat14 ="Bài hát 14";
        ArrayList<String> danhsachbaihat1 = new ArrayList<String>();
        danhsachbaihat1.add(baihat1);
        danhsachbaihat1.add(baihat2);
        danhsachbaihat1.add(baihat3);
        danhsachbaihat1.add(baihat6);
        danhsachbaihat1.add(baihat10);
        Album album1 = new Album("Tuyển tập album hay nhất năm 2001", danhsachbaihat1);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
         //////////////////////// album2//////////////////
     
         ArrayList<String> danhsachbaihat2 = new ArrayList<String>();
         danhsachbaihat2.add(baihat11);
         danhsachbaihat2.add(baihat3);
         danhsachbaihat2.add(baihat4);
         danhsachbaihat2.add(baihat5);
         danhsachbaihat2.add(baihat9);
         Album album2 = new Album("Tuyển tập album hay nhất năm 2002", danhsachbaihat2);
         ////////////////////////////////////////////////////////////////////////////////////////////////////////
         //////////////////////// album3//////////////////
     
         ArrayList<String> danhsachbaihat3 = new ArrayList<String>();
         danhsachbaihat3.add(baihat11);
         danhsachbaihat3.add(baihat3);
         danhsachbaihat3.add(baihat4);
         danhsachbaihat3.add(baihat5);
         danhsachbaihat3.add(baihat9);
         Album album3 = new Album("Tuyển tập album hay nhất năm 2003", danhsachbaihat3);
         ////////////////////////////////////////////////////////////////////////////////////////////////////////
           //////////////////////// album4//////////////////
     
           ArrayList<String> danhsachbaihat4 = new ArrayList<String>();
           danhsachbaihat4.add(baihat8);
           danhsachbaihat4.add(baihat2);
           danhsachbaihat4.add(baihat1);
           danhsachbaihat4.add(baihat12);
           danhsachbaihat4.add(baihat13);
           Album album4 = new Album("Tuyển tập album hay nhất năm 2004", danhsachbaihat4);
           ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////// album5//////////////////
     
            ArrayList<String> danhsachbaihat5 = new ArrayList<String>();
            danhsachbaihat5.add(baihat9);
            danhsachbaihat5.add(baihat10);
            danhsachbaihat5.add(baihat3);
            danhsachbaihat5.add(baihat14);
            danhsachbaihat5.add(baihat8);
            Album album5 = new Album("Tuyển tập album hay nhất năm 2005", danhsachbaihat5);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////// album6//////////////////
     
            ArrayList<String> danhsachbaihat6 = new ArrayList<String>();
            danhsachbaihat6.add(baihat10);
            danhsachbaihat6.add(baihat12);
            danhsachbaihat6.add(baihat3);
            danhsachbaihat6.add(baihat14);
            danhsachbaihat6.add(baihat8);
            danhsachbaihat6.add(baihat3);
            danhsachbaihat6.add(baihat5);
            danhsachbaihat6.add(baihat1);
            Album album6 = new Album("Tuyển tập album hay nhất năm 2006", danhsachbaihat6);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////// album7//////////////////
     
            ArrayList<String> danhsachbaihat7 = new ArrayList<String>();
            danhsachbaihat7.add(baihat11);
            danhsachbaihat7.add(baihat2);
            danhsachbaihat7.add(baihat3);
            danhsachbaihat7.add(baihat5);
            danhsachbaihat7.add(baihat7);
            danhsachbaihat7.add(baihat13);
            danhsachbaihat7.add(baihat8);
            danhsachbaihat7.add(baihat9);
            Album album7 = new Album("Tuyển tập album hay nhất năm 2007", danhsachbaihat7);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////// album8//////////////////
     
            ArrayList<String> danhsachbaihat8 = new ArrayList<String>();
            danhsachbaihat8.add(baihat3);
            danhsachbaihat8.add(baihat1);
            danhsachbaihat8.add(baihat9);
            danhsachbaihat8.add(baihat11);
            danhsachbaihat8.add(baihat10);
            danhsachbaihat8.add(baihat14);
            danhsachbaihat8.add(baihat2);
            danhsachbaihat8.add(baihat4);
            Album album8 = new Album("Tuyển tập album hay nhất năm 2008", danhsachbaihat8);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////// album8//////////////////
     
            ArrayList<String> danhsachbaihat9 = new ArrayList<String>();
            danhsachbaihat9.add(baihat2);
            danhsachbaihat9.add(baihat1);
            danhsachbaihat9.add(baihat9);
            danhsachbaihat9.add(baihat11);
            danhsachbaihat9.add(baihat10);
            danhsachbaihat9.add(baihat14);
            danhsachbaihat9.add(baihat2);
            danhsachbaihat9.add(baihat4);
            Album album9 = new Album("Tuyển tập album hay nhất năm 2009", danhsachbaihat9);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////// album8//////////////////
     
            ArrayList<String> danhsachbaihat10 = new ArrayList<String>();
            danhsachbaihat10.add(baihat2);
            danhsachbaihat10.add(baihat1);
            danhsachbaihat10.add(baihat9);
            danhsachbaihat10.add(baihat11);
            danhsachbaihat10.add(baihat10);
            danhsachbaihat10.add(baihat14);
            danhsachbaihat10.add(baihat2);
            danhsachbaihat10.add(baihat4);
            Album album10 = new Album("Tuyển tập album hay nhất năm 2022", danhsachbaihat10);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////


        ArrayList<Album> lstAlbums = new ArrayList<>();
        lstAlbums.add(album1);
        lstAlbums.add(album2);
        lstAlbums.add(album3);
        lstAlbums.add(album4);
        lstAlbums.add(album5);
        lstAlbums.add(album6);
        lstAlbums.add(album7);
        lstAlbums.add(album8);
        lstAlbums.add(album9);
        lstAlbums.add(album10);
        return lstAlbums;
    }



}
