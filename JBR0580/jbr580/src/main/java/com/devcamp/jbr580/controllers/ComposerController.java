package com.devcamp.jbr580.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr580.models.Artist;
import com.devcamp.jbr580.models.Band;
import com.devcamp.jbr580.models.Composer;
import com.devcamp.jbr580.services.ComposerService;


@RestController
@CrossOrigin
public class ComposerController {
    @Autowired
    private ComposerService composerService;
    @GetMapping(value="/composer")
    public ArrayList<Composer> getComposers() {

        ArrayList<Composer> composers = composerService.getComposer();
        return composers;
    }
    @GetMapping(value="/artist")
    public ArrayList<Artist> getArtist() {

        ArrayList<Artist> artists= composerService.getArtistList();
        return artists;
    }
    @GetMapping(value="/band")
    public ArrayList<Band> getBand() {

        ArrayList<Band> Bands= composerService.getBand();
        return Bands;
    }

    }
