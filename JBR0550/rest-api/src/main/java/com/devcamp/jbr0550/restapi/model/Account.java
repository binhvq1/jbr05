package com.devcamp.jbr0550.restapi.model;

public class Account {
    int id;
    Customer customer;
    double balance = 0.0;
    
    public Account() {
    }

    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    
}
