package com.devcamp.jbr0550.restapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr0550.restapi.model.Account;
import com.devcamp.jbr0550.restapi.model.Customer;
import com.devcamp.jbr0550.restapi.service.AccountService;
import com.devcamp.jbr0550.restapi.service.CustomerService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CustomerAccount {
    @Autowired
    private AccountService accountService;
    @GetMapping("/accounts")
    public ArrayList<Account> getAllAccount() {
        ArrayList<Account> allAccount = accountService.getAllAccount();
        return allAccount;
    }

    @Autowired
    private CustomerService customerService;
    @GetMapping("/customers")
    public ArrayList<Customer> getAllCustomer() {
        ArrayList<Customer> allCustomer = customerService.getAllCustomer();
        return allCustomer;
    }
}
