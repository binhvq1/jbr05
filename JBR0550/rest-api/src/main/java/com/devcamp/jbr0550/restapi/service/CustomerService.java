package com.devcamp.jbr0550.restapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.jbr0550.restapi.model.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer(1, "Hung", 10);
    Customer customer2 = new Customer(2, "Nam", 6);
    Customer customer3 = new Customer(3, "Phuong", 5);
    Customer customer4 = new Customer(4, "Hoai", 7);
    Customer customer5 = new Customer(5, "Bao", 10);
    Customer customer6 = new Customer(6, "Son", 9);

    public ArrayList<Customer> getAllCustomer() {
        ArrayList<Customer> customers = new ArrayList<Customer>();
        customers.add(customer1);
        customers.add(customer2);
        customers.add(customer3);
        customers.add(customer4);
        customers.add(customer5);
        customers.add(customer6);
        return customers;
    }
}
