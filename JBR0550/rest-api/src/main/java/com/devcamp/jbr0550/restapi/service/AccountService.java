package com.devcamp.jbr0550.restapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.jbr0550.restapi.model.Account;

@Service
public class AccountService {
    CustomerService customerService = new CustomerService();
    Account account1 = new Account(1, customerService.customer4, 1.0);
    Account account2 = new Account(2, customerService.customer5, 2.0);
    Account account3 = new Account(3, customerService.customer6, 3.0);

    public ArrayList<Account> getAllAccount() {
        ArrayList<Account> accounts = new ArrayList<Account>();
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        return accounts;
    }
}
