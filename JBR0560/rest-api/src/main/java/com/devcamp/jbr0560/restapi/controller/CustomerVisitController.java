package com.devcamp.jbr0560.restapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr0560.restapi.model.Customer;
import com.devcamp.jbr0560.restapi.model.Visit;
import com.devcamp.jbr0560.restapi.service.CustomerService;
import com.devcamp.jbr0560.restapi.service.VisitService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CustomerVisitController {
    @Autowired
    private VisitService visitService = new VisitService();
    @GetMapping("/visits")
    public ArrayList<Visit> getAllVisit() {
        ArrayList<Visit> allVisit = visitService.getAllVisit();
        return allVisit;
    }

    @GetMapping("/customers")
    public ArrayList<Customer> getAllCustomer() {
        CustomerService allCustomer = new CustomerService();
        return allCustomer.getAllCustomer();
    }
}
