package com.devcamp.jbr0560.restapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.jbr0560.restapi.model.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer("Hung", true, "vip");
    Customer customer2 = new Customer("Nam", true, "vip");
    Customer customer3 = new Customer("Hoang", true, "vip");
    Customer customer4 = new Customer("CHien", true, "vip");
    Customer customer5 = new Customer("Doanh", true, "vip");
    Customer customer6 = new Customer("Hien", true, "vip");

    public ArrayList<Customer> getAllCustomer() {
        ArrayList<Customer> customers = new ArrayList<Customer>();
        customers.add(customer1);
        customers.add(customer2);
        customers.add(customer3);
        customers.add(customer4);
        customers.add(customer5);
        customers.add(customer6);
        return customers;
    }
}
