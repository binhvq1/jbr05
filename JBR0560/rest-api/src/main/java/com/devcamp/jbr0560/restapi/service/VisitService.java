package com.devcamp.jbr0560.restapi.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.devcamp.jbr0560.restapi.model.Visit;

@Service
public class VisitService {
    CustomerService customerService = new CustomerService();
    Date date = new Date();
    Visit Visit1 = new Visit(customerService.customer1, date, 1.0, 1.0);
    Visit Visit2 = new Visit(customerService.customer2, date, 2.0, 2.0);
    Visit Visit3 = new Visit(customerService.customer3, date, 3.0, 3.0);

    public ArrayList<Visit> getAllVisit() {
        ArrayList<Visit> visits = new ArrayList<Visit>();
        visits.add(Visit1);
        visits.add(Visit2);
        visits.add(Visit3);
        return visits;
    }
}
