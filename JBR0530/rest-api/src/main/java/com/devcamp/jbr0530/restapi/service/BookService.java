package com.devcamp.jbr0530.restapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.jbr0530.restapi.model.Book;

@Service
public class BookService {
    AuthorService author1 = new AuthorService();
    Book book1 = new Book("Book1", author1.getAuthor1(), 10000, 100);

    AuthorService author2 = new AuthorService();
    Book book2 = new Book("Book2", author2.getAuthor2(), 20000, 200);

    AuthorService author3 = new AuthorService();
    Book book3 = new Book("Book3", author3.getAuthor3(), 30000, 300);

    public ArrayList<Book> getAllBook() {
        ArrayList<Book> books = new ArrayList<Book>();
        books.add(book1);
        books.add(book2);
        books.add(book3);
        return books;
    }
}
