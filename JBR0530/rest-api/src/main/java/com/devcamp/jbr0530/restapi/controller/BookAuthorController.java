package com.devcamp.jbr0530.restapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr0530.restapi.model.Author;
import com.devcamp.jbr0530.restapi.model.Book;
import com.devcamp.jbr0530.restapi.service.AuthorService;
import com.devcamp.jbr0530.restapi.service.BookService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class BookAuthorController {
    @Autowired
    private AuthorService authorService;
    @GetMapping("/authors")
    public ArrayList<Author> getAllAuthor() {
        ArrayList<Author> allAuthor = authorService.getAllAuthor();
        return allAuthor;
    }

    @Autowired
    private AuthorService authorServiceInfo;
    @GetMapping("/author-info")
    public Author getAuthorInfo(@RequestParam("email") String email) {
        ArrayList<Author> allAuthor = authorServiceInfo.getAllAuthor();
        Author findAuthor = new Author();
        for(Author author : allAuthor) {
            if(author.getEmail().equals(email)) {
                findAuthor = author;
            }
        }
        return findAuthor;
    }

    @Autowired
    private AuthorService authorServiceGender;
    @GetMapping("/author-gender")
    public Author getAuthorGender(@RequestParam("gender") char gender) {
        ArrayList<Author> allAuthor = authorServiceGender.getAllAuthor();
        Author findAuthor = new Author();
        for(Author author : allAuthor) {
            if(author.getGender() == gender) {
                findAuthor = author;
            }
        }
        return findAuthor;
    }

    @Autowired
    private BookService bookService;
    @GetMapping("/books")
    public ArrayList<Book> getAllBook() {
        ArrayList<Book> allBook = bookService.getAllBook();
        return allBook;
    }

    @Autowired
    private BookService bookService1;
    @GetMapping("/book-quantity-number")
    public Book getBookQuantityNumber(@RequestParam("quNumber") int qty) {
        ArrayList<Book> allBook = bookService1.getAllBook();

        Book findBook = new Book();

        for(Book book : allBook) {
            if(book.getQty() > qty) {
                findBook = book;
            }

        }
        return findBook;
    }
    
}
