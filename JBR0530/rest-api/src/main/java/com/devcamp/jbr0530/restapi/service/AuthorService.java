package com.devcamp.jbr0530.restapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.jbr0530.restapi.model.Author;

@Service
public class AuthorService {
    Author author1 = new Author("Hung", "Hung@gmail", 'm');
    Author author2 = new Author("Hong", "Hong@gmail", 'f');

    Author author3 = new Author("Nam", "Nam@gmail", 'm');
    Author author4 = new Author("Mai", "Mai@gmail", 'f');

    Author author5 = new Author("Tuan", "Tuan@gmail", 'm');
    Author author6 = new Author("Lan", "Lan@gmail", 'f');

    public ArrayList<Author> getAuthor1() {
        ArrayList<Author> listAllAuthor1 = new ArrayList<Author>();
        listAllAuthor1.add(author1);
        listAllAuthor1.add(author2);
        return listAllAuthor1;
    }

    public ArrayList<Author> getAuthor2() {
        ArrayList<Author> listAllAuthor2 = new ArrayList<Author>();
        listAllAuthor2.add(author3);
        listAllAuthor2.add(author4);
        return listAllAuthor2;
    }

    public ArrayList<Author> getAuthor3() {
        ArrayList<Author> listAllAuthor3 = new ArrayList<Author>();
        listAllAuthor3.add(author5);
        listAllAuthor3.add(author6);
        return listAllAuthor3;
    }
    public ArrayList<Author> getAllAuthor() {

        ArrayList<Author> listAllAuthor = new ArrayList<Author>();
        listAllAuthor.add(author1);
        listAllAuthor.add(author2);
        listAllAuthor.add(author3);
        listAllAuthor.add(author4);
        listAllAuthor.add(author5);
        listAllAuthor.add(author6);
        return listAllAuthor;
        
    }
}
